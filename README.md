# Event Planner - MDS

Un Event Planner

IDE -> Eclipse (https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2019-03/R/eclipse-inst-win64.exe)

ROADMAP - > https://docs.google.com/document/d/1hnR9ssfCo0DCY9rNWRrhToIJ6ND2vvO63vH9gnfpRk4/edit

Workflow : 
 - Pentru fiecare task pentru care vreti sa pushati (in cazul in care nu se specifica altfel) veti face un branch nou cu denumirea de tip "*tipul taskului*/issue-*numarul taskului*/*scurta descriere*"
        - spre exemplu: Pentru Issue #4, branchul va arata asa : "feature/issue-4/build-gui-as-a-mockup"
        - dupa ce constuiti branchul ve-ti commite si pusha modificari doar pe el pana terminati taskul respectiv
        - la finalul taskului (sau, in cazul in care este un task mai mare pe parcurs, cand terminati o etapa de dezvoltare) se va crea un merge request catre master
        - in cadrul merge requestului, ceilalti vor face, dupa posibilitati, review si vor da "LIKE" daca considera ca commiturile sunt bune si pot si merge-uite
        - nu dati "LIKE" la un merge-request daca nu ati facut review -> puteti sa dati "BIKINI"
        - nu dati merge la un merge-request daca nu aveti nici un "LIKE" pe el > rugati pe cineva sa arunce un ochi, macar o privire, sau in cazul in care commitul vostru ii impacteaza si pe altii, rugati-i pe cei in cazuza sa faca review
        - in cadrul reviewului, daca gasiti ceva ce considerati ca este in neregula cu un merge-request, comentati si asteptati raspuns de la owner
        - nu stergeti branchurile dupa ce dati merge in master -> lase-le sa fie acolo
 - Taskurile din "Open" sunt taskurile ce trebuiesc facute dar care nu au prioritate iminenta, cele din "To Do" trebuie facute primele, pe masura ce va terminati taskurile din "To Do", va rog sa vi le mutati singuri din "Open"
 - In timp ce lucrati la un Task acesta va sta in lista "Doing", iar dupa ce l-ati terminat (dupa ce ati merge-uit ultimul merge-request), va rog sa il mutati in "Closed"
 - NU PUSHATI DIRECT IN MASTER -> pushati pe branch si faceti merge-request (daca sunt modificari minore si sunteti sigur ca nu impacteaza cine stie ce, puteti sa dati merge si fara sa va fac nimeni review, ca sa nu pierdeti timpul)
 - Saptamanal incercam sa facem un sync in care sa vedem cine ce probleme a intampinat si ce alte taskuri ar trebui adaugate si cui