package com.event.planner;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class MyMenuBar {

	public static MenuBar getMenuBar() {
		// File Menu
		Menu fileMenu = new Menu("File");

		// File Menu Items
		MenuItem newItem = new MenuItem("New");
		fileMenu.getItems().add(newItem);
		newItem.setOnAction(e -> System.out.println("New"));
		MenuItem openItem = new MenuItem("Open");
		fileMenu.getItems().add(openItem);
		openItem.setOnAction(e -> System.out.println("Open"));
		MenuItem closeItem = new MenuItem("Close");
		fileMenu.getItems().add(closeItem);
		closeItem.setOnAction(e -> System.out.println("Close"));

		// Edit Menu
		Menu editMenu = new Menu("Edit");

		// Edit Menu Items
		MenuItem newItem1 = new MenuItem("New1");
		editMenu.getItems().add(newItem1);
		newItem1.setOnAction(e -> System.out.println("New1"));
		MenuItem openItem1 = new MenuItem("Open1");
		editMenu.getItems().add(openItem1);
		openItem1.setOnAction(e -> System.out.println("Open1"));
		MenuItem closeItem1 = new MenuItem("Close1");
		editMenu.getItems().add(closeItem1);
		closeItem1.setOnAction(e -> System.out.println("Close1"));

		// MenuBar
		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().add(fileMenu);
		menuBar.getMenus().add(editMenu);

		return menuBar;
	}

}
