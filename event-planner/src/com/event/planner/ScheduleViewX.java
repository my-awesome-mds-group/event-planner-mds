package com.event.planner;

import static com.event.planner.Constants.DAY_STRING;
import static com.event.planner.Constants.FX_BACKGROUND;
import static com.event.planner.Constants.FX_WHITE;
import static com.event.planner.Constants.FX_YELLOW;
import static com.event.planner.Constants.MONTH_STRING;
import static com.event.planner.Constants.WEEK_STRING;

import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class ScheduleViewX {
	
	private static Button monthButton;
	private static Button weekButton;
	private static Button dayButton;
	
	public static HBox getScheduleViewX() {
		monthButton = new  Button(MONTH_STRING);
		weekButton = new Button(WEEK_STRING);
		dayButton = new Button(DAY_STRING);
		VBox scheduleViewControl = new VBox(monthButton, weekButton, dayButton);
		scheduleViewControl.setStyle(FX_BACKGROUND + FX_WHITE);
		
		TextArea scheduleText = new TextArea();
		VBox.setVgrow(scheduleText, Priority.ALWAYS);
		VBox scheduleView = new VBox(scheduleText);
		HBox.setHgrow(scheduleView, Priority.ALWAYS);
		scheduleView.setStyle(FX_BACKGROUND + FX_YELLOW);
		HBox scheduleViewX = new HBox(scheduleView, scheduleViewControl);
		
		return scheduleViewX;
	}
}
