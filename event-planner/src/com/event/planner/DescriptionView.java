package com.event.planner;

import static com.event.planner.Constants.FX_BACKGROUND;
import static com.event.planner.Constants.FX_FUCHSIA;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class DescriptionView {

	public static VBox getDescriptionView() {
		TextArea descriptionText = new TextArea();
		descriptionText.setEditable(false);
		VBox descriptionView = new VBox(descriptionText);
		descriptionView.setStyle(FX_BACKGROUND + FX_FUCHSIA);
		VBox.setVgrow(descriptionText, Priority.ALWAYS);

		return descriptionView;
	}

}
