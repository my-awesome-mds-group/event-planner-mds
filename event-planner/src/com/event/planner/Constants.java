package com.event.planner;

public class Constants {
	public static final String FX_BACKGROUND = "-fx-background-color:";
	public static final String FX_RED = "rgba(0, 0, 0, 1);";
	public static final String FX_WHITE = "rgba(255, 255, 255, 1);";
	public static final String FX_YELLOW = "rgba(255, 255, 0, 1);";
	public static final String FX_FUCHSIA = "rgba(255, 0, 255, 1);";
	public static final String FX_BLACK = "rgba(0, 0, 0, 1);";
	public static final String FX_TURQOUISE = "rgba(255, 0, 255, 1);";
	
	public static final String INSERT_STRING = "Insert";
	public static final String EVENT_PLANNER_STRING = "Event Planner";
	public static final String MONTH_STRING = "Month";
	public static final String DAY_STRING = "Day";
	public static final String WEEK_STRING = "Week";
}
