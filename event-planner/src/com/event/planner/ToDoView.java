package com.event.planner;

import static com.event.planner.Constants.*;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class ToDoView {
	
	private static ListView<String> listView;
	public static VBox getToDoView() {
		TextField toDoInput = new TextField();
		Button toDoButton = new Button(INSERT_STRING);
		VBox addToDo = new VBox(toDoInput);
		VBox addToDoControl = new VBox(toDoButton);
		HBox addToDoX = new HBox(addToDo, addToDoControl);
		String addToDoXStyle = FX_BACKGROUND + FX_RED;
		addToDoX.setStyle(addToDoXStyle);
		listView = new ListView<>();
		listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		VBox addedList = new VBox(listView);
		VBox.setVgrow(addedList, Priority.ALWAYS);
		VBox.setVgrow(listView, Priority.ALWAYS);
		VBox toDoView = new VBox(addToDoX, addedList);
		toDoView.setStyle(FX_BACKGROUND + FX_TURQOUISE);
		return toDoView;
	}
}
