package com.event.planner;

import static com.event.planner.Constants.*;
import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class App extends Application {

	private Stage window;
	private Scene scene;
	
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		window = primaryStage;
		window.setTitle(EVENT_PLANNER_STRING);
		
		SplitPane info = new SplitPane(ScheduleViewX.getScheduleViewX(), DescriptionView.getDescriptionView());
		info.setOrientation(Orientation.VERTICAL);
		SplitPane content = new SplitPane(info, ToDoView.getToDoView());
		content.setOrientation(Orientation.HORIZONTAL);
		content.setDividerPositions(0.8);
		content.setStyle("-fx-resize: both;" + FX_BACKGROUND + FX_RED);
		
		info.maxWidthProperty().bind(content.widthProperty().multiply(0.8));
		
		
		VBox contentX = new VBox(MyMenuBar.getMenuBar(), content);
		contentX.setStyle("-fx-fill-width: true;");
		VBox.setVgrow(content, Priority.ALWAYS);
			
		GridPane temp = new GridPane();
		temp.add(contentX, 0, 0);
		GridPane.setHgrow(contentX, Priority.ALWAYS);
		GridPane.setVgrow(contentX, Priority.ALWAYS);
		temp.setStyle("-fx-fill-width: true;-fx-fill-height: true;" + FX_BACKGROUND + FX_BLACK);
		
		scene = new Scene(temp, 1000, 600);
		
		
		temp.setPrefHeight(scene.getHeight());
		window.setScene(scene);
		window.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}